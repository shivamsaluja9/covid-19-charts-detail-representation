﻿using CovidDataProject.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CovidDataProject.Service
{
    public class CovidService
    {
        /// <summary>
        ///  Data Upto Today In Donut Chart
        /// </summary>
        /// <returns></returns>
        public async  Task<CovidDailywiseMDL> GetCovidDonutDetails()
        {
            CovidDailywiseMDL objResult = new CovidDailywiseMDL();

            using (var client = new System.Net.Http.HttpClient())
            {
               //client.BaseAddress = new Uri("https://api.covid19india.org/data.json");
                //HTTP GET
                var result = await client.GetAsync("https://api.covid19india.org/data.json").ConfigureAwait(false); 
                

               // var  = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    var readTask = result.Content.ReadAsStringAsync();
                    objResult =JsonConvert.DeserializeObject<CovidDailywiseMDL>(readTask.Result);

                   
                }
            }
            return objResult;
        }


        /// <summary>
        /// State Wise Data Upto Today in Tabular form
        /// </summary>
        /// <returns></returns>
        public async Task<CovidStateWiseMDL> GetCovidStateWiseTabularDetails()
        {
            CovidStateWiseMDL objResult = new CovidStateWiseMDL();

            using (var client = new System.Net.Http.HttpClient())
            {
                //HTTP GET
                var result = await client.GetAsync("https://api.covid19india.org/states_daily.json").ConfigureAwait(false);


                // var  = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    var readTask = result.Content.ReadAsStringAsync();
                    objResult = JsonConvert.DeserializeObject<CovidStateWiseMDL>(readTask.Result);


                }
            }
            return objResult;
        }

    }
}