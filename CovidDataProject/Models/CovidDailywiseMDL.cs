﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CovidDataProject.Models
{
    public class CovidDailywiseMDL
    {
        public Cases_Time_Series[] cases_time_series { get; set; }
        public Statewise[] statewise { get; set; }
        public Tested[] tested { get; set; }
    }

    public class Cases_Time_Series
    {
        public string dailyconfirmed { get; set; }
        public string dailydeceased { get; set; }
        public string dailyrecovered { get; set; }
        public string date { get; set; }
        public string dateymd { get; set; }
        public string totalconfirmed { get; set; }
        public string totaldeceased { get; set; }
        public string totalrecovered { get; set; }
    }

    public class Statewise
    {
        public string active { get; set; }
        public string confirmed { get; set; }
        public string deaths { get; set; }
        public string deltaconfirmed { get; set; }
        public string deltadeaths { get; set; }
        public string deltarecovered { get; set; }
        public string lastupdatedtime { get; set; }
        public string migratedother { get; set; }
        public string recovered { get; set; }
        public string state { get; set; }
        public string statecode { get; set; }
        public string statenotes { get; set; }
    }

    public class Tested
    {
        public string dailyrtpcrsamplescollectedicmrapplication { get; set; }
        public string firstdoseadministered { get; set; }
        public string frontlineworkersvaccinated1stdose { get; set; }
        public string frontlineworkersvaccinated2nddose { get; set; }
        public string healthcareworkersvaccinated1stdose { get; set; }
        public string healthcareworkersvaccinated2nddose { get; set; }
        public string over45years1stdose { get; set; }
        public string over45years2nddose { get; set; }
        public string over60years1stdose { get; set; }
        public string over60years2nddose { get; set; }
        public string positivecasesfromsamplesreported { get; set; }
        public string registration1845years { get; set; }
        public string registrationabove45years { get; set; }
        public string registrationflwhcw { get; set; }
        public string registrationonline { get; set; }
        public string registrationonspot { get; set; }
        public string samplereportedtoday { get; set; }
        public string seconddoseadministered { get; set; }
        public string source { get; set; }
        public string source2 { get; set; }
        public string source3 { get; set; }
        public string source4 { get; set; }
        public string testedasof { get; set; }
        public string testsconductedbyprivatelabs { get; set; }
        public string to60yearswithcomorbidities1stdose { get; set; }
        public string to60yearswithcomorbidities2nddose { get; set; }
        public string totaldosesadministered { get; set; }
        public string totalindividualsregistered { get; set; }
        public string totalindividualstested { get; set; }
        public string totalindividualsvaccinated { get; set; }
        public string totalpositivecases { get; set; }
        public string totalrtpcrsamplescollectedicmrapplication { get; set; }
        public string totalsamplestested { get; set; }
        public string totalsessionsconducted { get; set; }
        public string updatetimestamp { get; set; }
        public string years1stdose { get; set; }
        public string years2nddose { get; set; }
    }

}
