﻿using CovidDataProject.Models;
using CovidDataProject.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CovidDataProject.Controllers
{
    public class CovidDetailsController : Controller
    {
        CovidService objCovidService = null;

        public CovidDetailsController()
        {
            objCovidService = new CovidService();
        }
        // GET: CovidDetails
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Get Donut Chart Details
        /// </summary>
        /// <returns></returns>
        public JsonResult GetDonutChartDetails()
        {

            var res = objCovidService.GetCovidDonutDetails().Result;
            Int64 TotalConfimedCases = res.cases_time_series.Sum(dr => Convert.ToInt64(dr.totalconfirmed));
            Int64 TotalDeceasedCases = res.cases_time_series.Sum(dr => Convert.ToInt64(dr.totaldeceased));
            Int64 TotalRecoverdCases = res.cases_time_series.Sum(dr => Convert.ToInt64(dr.totalrecovered));


            var DictData = new Dictionary<string, Int64>() {
                { "TotalConfimedCases",TotalConfimedCases },
                { "TotalDeceasedCases",TotalDeceasedCases },
                { "TotalRecoverdCases",TotalRecoverdCases },
            };

            return Json(DictData, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get State Wise Covid Details
        /// </summary>
        /// <returns></returns>
        public PartialViewResult GetCovidStateWiseDetails()
        {

            var ServiceResult = objCovidService.GetCovidStateWiseTabularDetails().Result;

            List<States_Daily> DataList = ServiceResult.states_daily
                            .GroupBy(c => c.status)
                            .Select(g => new States_Daily
                            {
                                status = g.Key,
                                an = g.Sum(c => Convert.ToInt64(c.an)).ToString(),
                                ap = g.Sum(c => Convert.ToInt64(c.ap)).ToString(),
                                ar = g.Sum(c => Convert.ToInt64(c.ar)).ToString(),
                                _as = g.Sum(c => Convert.ToInt64(c._as)).ToString(),
                                br = g.Sum(c => Convert.ToInt64(c.br)).ToString(),
                                ch = g.Sum(c => Convert.ToInt64(c.ch)).ToString(),
                                ct = g.Sum(c => Convert.ToInt64(c.ct)).ToString(),
                                dd = g.Sum(c => Convert.ToInt64(c.dd)).ToString(),
                                dl = g.Sum(c => Convert.ToInt64(c.dl)).ToString(),
                                dn = g.Sum(c => Convert.ToInt64(c.dn)).ToString(),
                                ga = g.Sum(c => Convert.ToInt64(c.ga)).ToString(),
                                gj = g.Sum(c => Convert.ToInt64(c.gj)).ToString(),
                                hp = g.Sum(c => Convert.ToInt64(c.hp)).ToString(),
                                hr = g.Sum(c => Convert.ToInt64(c.hr)).ToString(),
                                jh = g.Sum(c => Convert.ToInt64(c.jh)).ToString(),
                                jk = g.Sum(c => Convert.ToInt64(c.jk)).ToString(),
                                kl = g.Sum(c => Convert.ToInt64(c.kl)).ToString(),
                                la = g.Sum(c => Convert.ToInt64(c.la)).ToString(),
                                ld = g.Sum(c => Convert.ToInt64(c.ld)).ToString(),
                                mh = g.Sum(c => Convert.ToInt64(c.mh)).ToString(),
                                ml = g.Sum(c => Convert.ToInt64(c.ml)).ToString(),
                                mn = g.Sum(c => Convert.ToInt64(c.mn)).ToString(),
                                mp = g.Sum(c => Convert.ToInt64(c.mp)).ToString(),
                                mz = g.Sum(c => Convert.ToInt64(c.mz)).ToString(),
                                nl = g.Sum(c => Convert.ToInt64(c.nl)).ToString(),
                                or = g.Sum(c => Convert.ToInt64(c.or)).ToString(),
                                pb = g.Sum(c => Convert.ToInt64(c.pb)).ToString(),
                                py = g.Sum(c => Convert.ToInt64(c.py)).ToString(),
                                rj = g.Sum(c => Convert.ToInt64(c.rj)).ToString(),

                                sk = g.Sum(c => Convert.ToInt64(c.sk)).ToString(),
                                tg = g.Sum(c => Convert.ToInt64(c.tg)).ToString(),
                                tn = g.Sum(c => Convert.ToInt64(c.tn)).ToString(),
                                tr = g.Sum(c => Convert.ToInt64(c.tr)).ToString(),
                                tt = g.Sum(c => Convert.ToInt64(c.tt)).ToString(),
                                un = g.Sum(c => Convert.ToInt64(c.un)).ToString(),
                                up = g.Sum(c => Convert.ToInt64(c.up)).ToString(),
                                ut = g.Sum(c => Convert.ToInt64(c.ut)).ToString(),
                                wb = g.Sum(c => Convert.ToInt64(c.wb)).ToString(),

                            }).ToList();

            return PartialView("_TabularDataView",DataList);
        }
    }

}